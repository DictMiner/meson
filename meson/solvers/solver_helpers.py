from abc import ABC, abstractmethod

from functools import wraps


# pylint: disable=C0103
class AbstractSolver(ABC):

    @abstractmethod
    def predict(self, X, **kwargs):
        pass

    @abstractmethod
    def fit(self, X, y, X_val=None, y_val=None, **kwargs):
        pass

    @abstractmethod
    def save(self, path="", **kwargs):
        pass

    @abstractmethod
    def load(self, path="", **kwargs):
        pass


def singleton(cls):
    instance = None

    @wraps(cls)
    def inner(*args, **kwargs):
        nonlocal instance
        if instance is None:
            instance = cls(*args, **kwargs)
        return instance

    return inner
