""" Forward-Chaining CV Classifier """

import pandas as pd


def train_valid_split(data, offset=pd.offsets.DateOffset(years=1)):
    split_dt = data.index.get_level_values('date').max() - offset

    train_idx = data.index.get_level_values('date') <= split_dt
    valid_idx = data.index.get_level_values('date') > split_dt

    return train_idx, valid_idx


# pylint: disable=C0103, W0613
class TimeTuneSolver:
    """
    Cross Validation Solver
    """
    def __init__(self, solver, optimizer=None, **kwargs):
        self.solver_ = solver
        self.model_ = None
        self.optimizer_ = optimizer
        self.classes_ = [0, 1]


    def fit(self, X, y, **kwargs):
        valid_size = 180000

        best_params = {}
        if self.optimizer_ is not None:
            _, best_params = self.optimizer_(X[:-valid_size], y[:-valid_size],
                                             X[-valid_size:], y[-valid_size:])
        model = self.solver_(**best_params)
        model.fit(X[:-valid_size], y[:-valid_size], X[-valid_size:], y[-valid_size:], verbose=False)

        best_params = model.get_best_params()
        self.model_ = self.solver_(**best_params)
        self.model_.fit(X, y)


    def get_params(self, **kwargs):
        return {"solver": self.solver_}


    def predict(self, X, **kwargs):
        result = self.model_.predict(X, **kwargs)
        return result


    def predict_proba(self, X, **kwargs):
        result = self.model_.predict_proba(X, **kwargs)
        return result
