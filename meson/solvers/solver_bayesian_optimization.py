import time

from bayes_opt import BayesianOptimization


# pylint: disable=C0103
def bayesopt(X, y, X_val, y_val, solver, space, space_to_params,
             init_points=5, opt_time=60, verbose=False, random_state=42):
    start_time = time.time()
    print("Start bayesian optimization at ", time.ctime())

    def objective(**hyperparams) -> float:
        model = solver(**space_to_params(hyperparams))
        model.fit(X, y, X_val, y_val, verbose=False)
        score = model.get_best_score()
        return -score

    bopt = BayesianOptimization(f=objective,
                                pbounds=space,
                                verbose=verbose,
                                random_state=random_state)
    bopt.maximize(init_points=init_points, n_iter=0)
    i = 0
    while time.time()-start_time <= opt_time:
        bopt.maximize(init_points=0, n_iter=1)
        i += 1
    print("Bopt time limit after {} iterations!".format(i+1))

    best_score = -bopt.max['target']
    best_params = space_to_params(bopt.max["params"])

    print("Best bopt score: {}".format(best_score))
    print("Best bopt params: ", best_params)
    return best_score, best_params
