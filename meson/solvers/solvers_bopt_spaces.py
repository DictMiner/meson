""" Solvers spaces for bayesian optimization"""

solver13_space = {
        "learning_rate": (0.01, 0.5),
        "max_depth": (3, 10),
        "rsm": (0.4, 1),
        "subsample": (0.4, 1),
        "reg_lambda": (0, 100),
    }

def solver13_space_to_params(sample):
    params = sample.copy()

    if "max_depth" in sample:
        params["max_depth"] = int(sample["max_depth"])

    return params
