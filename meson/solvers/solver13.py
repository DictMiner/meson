""" Catboost Classifier """

import os
from catboost import CatBoostClassifier
from catboost.utils import eval_metric
from .solver_helpers import AbstractSolver


# pylint: disable=C0103
class Solver(AbstractSolver):
    """
    CatBoost Classification Solver
    """
    def __init__(self, model_path=None, load_from_path=False, random_state=42, **kwargs):
        self.model_path_ = model_path
        if (self.model_path_ is not None) and not os.path.exists(self.model_path_):
            os.makedirs(self.model_path_)

        self.model_ = CatBoostClassifier(random_state=random_state, **kwargs)
        if load_from_path:
            self.load(self.model_path_)


    def fit(self, X, y, X_val=None, y_val=None, **kwargs):
        if X_val is not None:
            params = {
                **{
                    'use_best_model':True,
                    'eval_set':(X_val, y_val),
                    'verbose':False,
                    'early_stopping_rounds':20,
                    'plot':False
                },
                **kwargs
            }
        else:
            params = {
                **{
                    'verbose':False,
                    'plot':False,
                },
                **kwargs
            }
        self.model_.fit(X, y, **params)


    def get_best_score(self, ):
        metric = self.model_.get_all_params()["eval_metric"]
        return self.model_.get_best_score()['validation'][metric]


    def get_params(self, **kwargs):
        return self.model_.get_params()


    def get_best_params(self, ):
        params = self.model_.get_params()
        if self.model_.get_best_iteration() is not None:
            params["iterations"] = self.model_.get_best_iteration()
        params["learning_rate"] = self.model_.get_all_params()["learning_rate"]
        return params


    def predict(self, X, **kwargs):
        result = self.model_.predict(X, **kwargs)
        return result


    def predict_proba(self, X, **kwargs):
        result = self.model_.predict_proba(X, **kwargs)
        return result


    def score(self, X, y, metric="Logloss", **kwargs):
        predictions = self.predict_proba(X)
        return eval_metric(y, predictions, metric, **kwargs)


    def save(self, path=None, **kwargs):
        path = path or self.model_path_
        if os.path.isdir(path):
            path = os.path.join(path, "catboost_classifier_model")
        self.model_.save_model(path, **kwargs)


    def load(self, path=None, **kwargs):
        path = path or self.model_path_
        if os.path.isdir(path):
            path = os.path.join(path, "catboost_classifier_model")
        self.model_.load_model(path, **kwargs)
