import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.calibration import calibration_curve
from sklearn.metrics import log_loss, accuracy_score, roc_auc_score, brier_score_loss


def load_data(file_name, data_dir="./data"):
    with open(os.path.join(data_dir, file_name), 'rb') as handle:
        data = pd.read_pickle(handle)
    print("Number of records {:d} from {} to {} for {:d} companies".format(
        len(data),
        data.index.get_level_values('date').min().date(),
        data.index.get_level_values('date').max().date(),
        data.index.get_level_values('tradingitemid').nunique()
        ))
    return data

def plot_feature_changes(feature, data, tradingitemid=None):
    if tradingitemid is None:
        tradingitemid = np.random.choice(data.index.get_level_values('tradingitemid'))
    data.xs(tradingitemid, level="tradingitemid")[feature].plot(
        title="tradingitemid = {:d}".format(tradingitemid),
        ylabel=feature,
        marker='o',
        figsize=(12, 4)
        )


def check_data_consistency(train, test, labels):
    is_consistent = True
    if (train.index.values != labels.index.values).any():
        print("Caution! Train and labels indexes mismatch!")
        is_consistent = False

    if labels.isnull().any():
        print("Caution! One of train labels is null!")
        is_consistent = False

    if (train.dtypes != test.dtypes).any():
        print("Caution! Train and test column types mismatch!")
        is_consistent = False

    if any([i > j for i, j in zip(train.index.get_level_values('date')[:-1],
                                  train.index.get_level_values('date')[1:])]):
        print("Train data is not sorted!")
        is_consistent = False

    if is_consistent:
        print("All data is consistent!")
        print("Column types:", train.dtypes.value_counts().to_json()[1:-1])


def train_test_split(data, train_dt, test_offset=pd.offsets.DateOffset(years=1),
                                     train_offset=None):
    train_dt = pd.Timestamp(train_dt)
    test_dt = train_dt + test_offset
    if train_offset is None:
        train_idx = data.index.get_level_values('date') <= train_dt
    else:
        train_start_dt = train_dt - train_offset
        train_idx = (train_start_dt < data.index.get_level_values('date')) & \
                    (data.index.get_level_values('date') <= train_dt)

    test_idx = (train_dt < data.index.get_level_values('date')) & \
               (data.index.get_level_values('date') <= test_dt)
    print("For {} n samples:\t train: {:d}({:.0f}%),\t test: {:d}({:.0f}%)".format(
        train_dt.date(),
        train_idx.sum(),
        100*train_idx.sum()/len(data),
        test_idx.sum(),
        100*test_idx.sum()/len(data),
        ))
    return train_idx, test_idx


def calculate_metrics(y_labels, prob_pos):
    print("Log loss:\t {:.3f}".format(log_loss(y_labels, prob_pos)))
    print("Brier score:\t {:.3f}".format(brier_score_loss(y_labels, prob_pos)))
    print("Accuracy:\t {:.3f}".format(accuracy_score(y_labels, prob_pos > 0.5)))
    print("Roc auc:\t {:.3f}".format(roc_auc_score(y_labels, prob_pos)))


def plot_calibration_plot(y_labels, prob_pos, name="classifier", n_bins=10, **kwargs):
    prob_true, prob_pred = calibration_curve(y_labels, prob_pos, n_bins=n_bins, **kwargs)

    plt.figure(figsize=(10, 10))
    ax1 = plt.subplot2grid((3, 1), (0, 0), rowspan=2)
    ax2 = plt.subplot2grid((3, 1), (2, 0))

    ax1.plot([0, 1], [0, 1], "k:", label="Perfectly calibrated")

    ax1.plot(prob_pred, prob_true, "-", label="%s" % (name, ))
    ax2.hist(prob_pos, range=(0, 1), bins=n_bins, label=name, histtype="step", lw=2)

    ax1.set_ylabel("Fraction of positives")
    ax1.set_ylim([-0.05, 1.05])
    ax1.legend(loc="lower right")
    ax1.set_title('Calibration plots  (reliability curve)')

    ax2.set_xlabel("Mean predicted value")
    ax2.set_ylabel("Count")
    ax2.legend(loc="upper center", ncol=2)

    plt.show()
